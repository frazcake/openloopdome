#pragma once

#include "indidome.h"

/**
 * @brief The OpenLoopDome class provides an absolute position dome without encoders that supports parking, unparking, and slaving.
 */
class OpenLoopDome : public INDI::Dome
{
  public:
    OpenLoopDome();
    virtual ~OpenLoopDome() = default;

    virtual bool ISNewNumber(const char *dev, const char *name, double values[], char *names[], int n) override;
    virtual bool ISSnoopDevice(XMLEle *root) override;
    virtual bool ISNewSwitch(const char *dev, const char *name, ISState *states, char *names[], int n) override;

    virtual bool initProperties() override;
    const char *getDefaultName();
    bool updateProperties() override;

  protected:
    bool Connect();
    bool Disconnect();

    void KeepAliveShutter();
    void ShutterRoutine();
    void StartEngine();
    void StopEngine();
    void RotationRoutine();
    double AccelRotation();
    double ConstantSpeedRotation();
    void DeltaPosition(double deltaPos);

    void TimerHit();

    virtual IPState Move(DomeDirection dir, DomeMotionCommand operation);
    virtual IPState MoveRel(double azDiff);
    virtual IPState MoveAbs(double az);
    virtual IPState Park();
    virtual IPState UnPark();
    virtual IPState ControlShutter(ShutterOperation operation);
    virtual bool Abort();

    // Parking
    virtual bool SetCurrentPark();
    virtual bool SetDefaultPark();

  private:
    INumber SyncPositionN[1];
    INumberVectorProperty SyncPositionNP;
    ISwitch SpegniS[3];
    ISwitchVectorProperty SpegniSP;
    ISwitch BluetoothS[1];
    ISwitchVectorProperty BluetoothSP;
    ISwitch RiavviaPiS[1];
    ISwitchVectorProperty RiavviaPiSP;
    double targetAz;
    double shutterTimer;
    bool SetupParms();
    int TimeSinceUpdate;
};
