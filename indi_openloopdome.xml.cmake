<?xml version="1.0" encoding="UTF-8"?>
<driversList>
<devGroup group="Domes">
	<device label="Open Loop Dome">
                <driver name="Open Loop Dome">indi_openloopdome</driver>
                <version>@INDI_OPENLOOPDOME_VERSION_MAJOR@.@INDI_OPENLOOPDOME_VERSION_MINOR@</version>
	</device>
</devGroup>
</driversList>
