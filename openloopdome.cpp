#include "openloopdome.h"

#include "indicom.h"
#include <string.h>
#include <cmath>
#include <memory>
#include <unistd.h>
#include <wiringPi.h>
#include <wiringSerial.h>

// We declare an auto pointer to OpenLoopDome.
std::unique_ptr<OpenLoopDome> openLoopDome(new OpenLoopDome());

#define SHUTTER_TIMER 50.0  /* Shutter closes/open in 50 seconds */
#define REED_0_PIN 12
#define REED_90_PIN 16
#define REED_180_PIN 20
#define REED_270_PIN 21
#define MOTOR_WISE_PIN 18
#define MOTOR_ACTIVATION_PIN 4
#define SPEGNI_PRIMO_PIN 19
#define SPEGNI_SECONDO_PIN 26
#define DOME_SPEED 3.072   /* degrees per second, constant */
#define TIMER_LOOP 0.25 
#define ACC_TIME 1.5

//gestione motore
bool alreadyRunning = false;
double rotatingSecFromT0 = 0;
double goalTime = 0;
double distance = 0;
double distanceTreshold = 0;
double startingPosition = 0;
double distanceWise = 0;
//file descriptor per seriale bluetooth
int fd = -1;

//gestione shutter
double shutter_keep_alive_timer = 0;
int shutter_close_count = 0;
int shutter_open_count = 0;

//static void OpenLoopDome::reedRoutine0() {
//    DomeAbsPosN[0].value = 349.7;
//}
//
//void OpenLoopDome::reedRoutine90() {
//    DomeAbsPosN[0].value = 84;
//}
//
//void OpenLoopDome::reedRoutine180() {
//    DomeAbsPosN[0].value = 189.70;
//}
//
//void OpenLoopDome::reedRoutine270() {
//    DomeAbsPosN[0].value = 264;
//}

void ISPoll(void *p);

void ISGetProperties(const char *dev) {
    openLoopDome -> ISGetProperties(dev);
}

void ISNewSwitch(const char *dev,
    const char *name, ISState *states, char *names[], int n) {
    openLoopDome -> ISNewSwitch(dev, name, states, names, n);
}

void ISNewText(const char *dev,
    const char *name, char *texts[], char *names[], int n) {
    openLoopDome -> ISNewText(dev, name, texts, names, n);
}

void ISNewBLOB(const char *dev,
    const char *name, int sizes[], int blobsizes[], char *blobs[], char *formats[], char *names[], int n) {
    INDI_UNUSED(dev);
    INDI_UNUSED(name);
    INDI_UNUSED(sizes);
    INDI_UNUSED(blobsizes);
    INDI_UNUSED(blobs);
    INDI_UNUSED(formats);
    INDI_UNUSED(names);
    INDI_UNUSED(n);
}

void ISNewNumber(const char *dev,
    const char *name, double values[], char *names[], int n) {
    openLoopDome -> ISNewNumber(dev, name, values, names, n);
}

void ISSnoopDevice(XMLEle *root) {
    openLoopDome -> ISSnoopDevice(root);
}

OpenLoopDome::OpenLoopDome() {
    SetDomeCapability(DOME_CAN_ABORT | DOME_CAN_ABS_MOVE | DOME_CAN_REL_MOVE | DOME_CAN_PARK | DOME_HAS_SHUTTER);
}

bool OpenLoopDome::initProperties() {
    INDI::Dome::initProperties();
    targetAz = 0;
    shutterTimer = 0;
    prev_az = 0;
    prev_alt = 0;
    TimeSinceUpdate = 0;
    alreadyRunning = false;
    DomeAbsPosN[0].value = 180;
    wiringPiSetupSys();
//    wiringPiISR(REED_0_PIN, INT_EDGE_RISING, &OpenLoopDome::reedRoutine0);
//    wiringPiISR(REED_90_PIN, INT_EDGE_RISING, &OpenLoopDome::reedRoutine90);
//    wiringPiISR(REED_180_PIN, INT_EDGE_RISING, &OpenLoopDome::reedRoutine180);
//    wiringPiISR(REED_270_PIN, INT_EDGE_RISING, &OpenLoopDome::reedRoutine270);
    IUFillNumber(&SyncPositionN[0], "Sync real position", "", "%6.2f", 0.0, 360.0, 1.0, 180.0);
    IUFillNumberVector(&SyncPositionNP, SyncPositionN, 1, getDeviceName(), "SYNC_POSITION", "Sync position", MAIN_CONTROL_TAB, IP_RW, 60, IPS_OK);
    IUFillSwitch(&SpegniS[0], "Spegni tutto", "Spegni tutto", ISS_OFF);
    IUFillSwitch(&SpegniS[1], "Accendi Cupola", "Alimenta Cupola", ISS_OFF);
    IUFillSwitch(&SpegniS[2], "Accendi tutto", "Alimenta tutto", ISS_OFF);
    IUFillSwitchVector(&SpegniSP, SpegniS, 3, getDeviceName(), "Alimentazione Osservatorio", "Alimentazione", MAIN_CONTROL_TAB, IP_RW, ISR_1OFMANY, 0, IPS_OK);
    IUFillSwitch(&BluetoothS[0], "Riconnetti BT", "Riconnetti BT", ISS_OFF);
    IUFillSwitchVector(&BluetoothSP, BluetoothS, 1, getDeviceName(), "Riconnetti BT", "Riconnetti BT", MAIN_CONTROL_TAB, IP_RW, ISR_1OFMANY, 0, IPS_OK);
    IUFillSwitch(&RiavviaPiS[0], "Riavvia Sistema", "Riavvia Sistema", ISS_OFF);
    IUFillSwitchVector(&RiavviaPiSP, RiavviaPiS, 1, getDeviceName(), "Riavvia Sistema", "Riavvia Sistema", MAIN_CONTROL_TAB, IP_RW, ISR_1OFMANY, 0, IPS_OK);
    SetParkDataType(PARK_AZ);
    addAuxControls();
    return true;
}

bool OpenLoopDome::SetupParms() {
    DomeParamN[0].value = 5;
    IDSetNumber(&DomeAbsPosNP, nullptr);
    IDSetNumber(&DomeParamNP, nullptr);
    if (InitPark()) {
        // If loading parking data is successful, we just set the default parking values.
        SetAxis1ParkDefault(180);
    } else {
        // Otherwise, we set all parking data to default in case no parking data is found.
        SetAxis1Park(180);
        SetAxis1ParkDefault(180);
    }
    loadConfig(false);
    return true;
}

const char *OpenLoopDome::getDefaultName() {
    return (const char *) "Open Loop Dome";
}

bool OpenLoopDome::updateProperties() {
    INDI::Dome::updateProperties();
    if (isConnected()) {
        SetupParms();
        defineNumber(&SyncPositionNP);
        defineSwitch(&SpegniSP);
        defineSwitch(&BluetoothSP);
        defineSwitch(&RiavviaPiSP);
    } else {
        deleteProperty(SyncPositionNP.name);
        deleteProperty(SpegniSP.name);
        deleteProperty(BluetoothSP.name);
        deleteProperty(RiavviaPiSP.name);
    }
    return true;
}

bool OpenLoopDome::Connect() {
    SetTimer(TIMER_LOOP * 1000);
    return true;
}

bool OpenLoopDome::Disconnect() {
    return true;
}

void OpenLoopDome::ShutterRoutine(){
    if (alreadyRunning)
        StopEngine(); 
    if (DomeShutterS[0].s == ISS_ON && shutter_open_count < 3) {
        fd = serialOpen("/dev/rfcomm0", 9600);
        shutterTimer = 0;
        serialPuts(fd, "O\n");
        shutter_open_count++;
        shutter_close_count = 0;
        serialClose(fd);
    } else if (DomeShutterS[0].s == ISS_OFF && shutter_close_count < 3) {
        fd = serialOpen("/dev/rfcomm0", 9600);
        shutterTimer = 0;
        serialPuts(fd, "C\n");
        shutter_close_count++;
        shutter_open_count = 0;
        serialClose(fd);
    }
    if (shutterTimer++ >= SHUTTER_TIMER / TIMER_LOOP) {
        shutter_open_count = 0;
        shutter_close_count = 0;
        DomeShutterSP.s = IPS_OK;
        LOGF_INFO("Shutter is %s.", (DomeShutterS[0].s == ISS_ON ? "open" : "closed"));
        IDSetSwitch(&DomeShutterSP, nullptr);
    }
    return;
}

void OpenLoopDome::KeepAliveShutter(){
    if (DomeShutterS[0].s == ISS_ON && DomeShutterSP.s == IPS_OK) {
        if (shutter_keep_alive_timer++ > 60 / TIMER_LOOP) {
            fd = serialOpen("/dev/rfcomm0", 9600);
            serialPuts(fd, "K\n");
            shutter_keep_alive_timer = 0;
            serialClose(fd);
        }
    }
    return;
}

void OpenLoopDome::RotationRoutine(){
    if(alreadyRunning){
        rotatingSecFromT0 += TIMER_LOOP;
        if(distance > distanceTreshold){ //se maggiore soglia
            if(rotatingSecFromT0 <= ACC_TIME)
                DeltaPosition(AccelRotation());
            else if(rotatingSecFromT0 <= goalTime){
                DeltaPosition(ConstantSpeedRotation());
            }
        } 
        else if(rotatingSecFromT0 <= goalTime) //else ==> se minore della soglia, if ==> se minore del goaltime
            DeltaPosition(AccelRotation());
        if(rotatingSecFromT0 >= goalTime){ //ho raggiunto il tempo necessario
            DomeAbsPosN[0].value = targetAz;
            StopEngine();
        }
    } else {
        startingPosition = DomeAbsPosN[0].value;
        distanceTreshold = DOME_SPEED * ACC_TIME;
        distanceWise = fmod((targetAz - DomeAbsPosN[0].value + 360), 360.0);
        distance = 180.0 - fabs(fmod(fabs(targetAz - DomeAbsPosN[0].value), 360.0) - 180.0);
        IUResetSwitch(&DomeMotionSP);
	if(distanceWise <= 180) {
	    digitalWrite(MOTOR_WISE_PIN, HIGH);
        DomeMotionS[DOME_CCW].s = ISS_OFF;
        DomeMotionS[DOME_CW].s = ISS_ON;
	}
	else {
	    digitalWrite(MOTOR_WISE_PIN, LOW);
        DomeMotionS[DOME_CW].s = ISS_OFF;
        DomeMotionS[DOME_CCW].s = ISS_ON;
	}
	IDSetSwitch(&DomeMotionSP, nullptr);
        if(distance > distanceTreshold)
            goalTime = ACC_TIME + (distance - distanceTreshold) / DOME_SPEED;
        else
            goalTime = ACC_TIME * sqrt(distance / distanceTreshold);
        digitalWrite(MOTOR_ACTIVATION_PIN, LOW);
        alreadyRunning = true;
    }
}

void OpenLoopDome::StopEngine(){
    digitalWrite(MOTOR_ACTIVATION_PIN, HIGH);
    digitalWrite(MOTOR_WISE_PIN, HIGH);
    alreadyRunning = false;
    goalTime = 0;
    rotatingSecFromT0 = 0;
    distance = 0;
    if (getDomeState() == DOME_PARKING) {
        SetParked(true);
    } else if (getDomeState() == DOME_UNPARKING)
        SetParked(false);
    else
        setDomeState(DOME_SYNCED);
}

double OpenLoopDome::AccelRotation(){
    return pow(rotatingSecFromT0, 2) * DOME_SPEED / (2 * ACC_TIME); 
}

double OpenLoopDome::ConstantSpeedRotation(){
    return ACC_TIME * DOME_SPEED / 2 + DOME_SPEED * (rotatingSecFromT0 - ACC_TIME);
}

void OpenLoopDome::DeltaPosition(double deltaPos){
    if(DomeMotionS[DOME_CW].s == ISS_ON)
        DomeAbsPosN[0].value = startingPosition + deltaPos;
    else if(DomeMotionS[DOME_CCW].s == ISS_ON)
        DomeAbsPosN[0].value = startingPosition - deltaPos;
    DomeAbsPosN[0].value = range360(DomeAbsPosN[0].value);
    IDSetNumber(&DomeAbsPosNP, nullptr);
}

void OpenLoopDome::TimerHit() {
    if (!isConnected())
        return; //  No need to reset timer if we are not connected anymore
    KeepAliveShutter();
    if (DomeShutterSP.s == IPS_BUSY)
        ShutterRoutine();    
    else if (DomeAbsPosNP.s == IPS_BUSY) 
        RotationRoutine();
    SetTimer(TIMER_LOOP * 1000);
    if (!isParked() && TimeSinceUpdate++ > 10 / TIMER_LOOP) {
        TimeSinceUpdate = 0;
        UpdateMountCoords();
    }
}

IPState OpenLoopDome::Move(DomeDirection dir, DomeMotionCommand operation) {
    if (alreadyRunning && operation == MOTION_START) {
        LOG_INFO("Tentativo di inversione di rotazione senza prima fermare la cupola. Azione non sicura. Fermo io la cupola.");
        LOG_INFO("Riprovare ora.");
        targetAz = DomeAbsPosN[0].value;
        return IPS_BUSY;
    }
    if (operation == MOTION_START) {
        targetAz = (dir == DOME_CW) ? (range360(DomeAbsPosN[0].value + 180)) : (range360(DomeAbsPosN[0].value - 179)); //1e6 : -1e6;
        DomeAbsPosNP.s = IPS_BUSY;
    } else {
        rotatingSecFromT0 = goalTime;
        targetAz = DomeAbsPosN[0].value;
    }
    IDSetNumber(&DomeAbsPosNP, nullptr);
    return ((operation == MOTION_START) ? IPS_BUSY : IPS_OK);
}

IPState OpenLoopDome::MoveAbs(double az) {
    targetAz = az;
    if (fabs(az - DomeAbsPosN[0].value) < DOME_SPEED * TIMER_LOOP / 2)
        return IPS_OK;
    return IPS_BUSY;
}

IPState OpenLoopDome::MoveRel(double azDiff) {
    targetAz = DomeAbsPosN[0].value + azDiff;
    if (targetAz < DomeAbsPosN[0].min)
        targetAz += DomeAbsPosN[0].max;
    if (targetAz > DomeAbsPosN[0].max)
        targetAz -= DomeAbsPosN[0].max;
    if (fabs(targetAz - DomeAbsPosN[0].value) < DOME_SPEED * TIMER_LOOP / 2)
        return IPS_OK;
    return IPS_BUSY;
}

IPState OpenLoopDome::Park() {
    if (INDI::Dome::isLocked()) {
        LOG_INFO("Cannot Park Dome when mount is locking. See: Telescope parking policy, in options tab");
        return IPS_ALERT;
    }
    Dome::ControlShutter(SHUTTER_CLOSE);
    targetAz = DomeParamN[0].value;
    Dome::MoveAbs(GetAxis1Park());
    return IPS_BUSY;
}

IPState OpenLoopDome::UnPark() {
    return IPS_OK; 
}

IPState OpenLoopDome::ControlShutter(ShutterOperation operation) {
    //da investigare per cloudwatcher
    INDI_UNUSED(operation);
    return IPS_BUSY;
}

bool OpenLoopDome::Abort() {
    StopEngine();
    LOGF_INFO("Attempting to abort dome motion by stopping at %g", DomeAbsPosN[0].value);
    targetAz = DomeAbsPosN[0].value;
    DomeAbsPosNP.s = IPS_OK;
    return true;
}

bool OpenLoopDome::SetCurrentPark() {
    SetAxis1Park(DomeAbsPosN[0].value);
    return true;
}

bool OpenLoopDome::SetDefaultPark() {
    // By default set position to 180
    SetAxis1Park(180);
    return true;
}

bool OpenLoopDome::ISNewNumber(const char *dev,
    const char *name, double values[], char *names[], int n) {
    if (strcmp(dev, getDeviceName()))
        return false;
    if (!strcmp(name, SyncPositionNP.name)) {
        if (IUUpdateNumber(&SyncPositionNP, values, names, n) < 0)
            return false;
        if (values[0] >= 0 && values[0] < 360) {
            DomeAbsPosN -> value = values[0];
            IDSetNumber(&SyncPositionNP, "Sincronizzazione con posizione attuale riuscita");
            IDSetNumber(&DomeAbsPosNP, "Sincronizzazione con posizione attuale riuscita");
            return true;
        }
    }
    return INDI::Dome::ISNewNumber(dev, name, values, names, n);
}

bool OpenLoopDome::ISNewSwitch(const char *dev,
    const char *name, ISState *states, char *names[], int n) {
    if (strcmp(dev, getDeviceName()))
        return false;
    if (!strcmp(name, SpegniSP.name)) {
        IUUpdateSwitch(&SpegniSP, states, names, n);
        if (SpegniS[0].s == ISS_ON) {
            digitalWrite(SPEGNI_PRIMO_PIN, LOW);
            LOG_INFO("Sto spegnendo tutto");
            IDSetSwitch(&SpegniSP, "Sto spegnendo tutto.");
        } else if (SpegniS[1].s == ISS_ON) {
            digitalWrite(SPEGNI_PRIMO_PIN, HIGH);
            LOG_INFO("Sto accendendo la cupola");
            IDSetSwitch(&SpegniSP, "Sto accendendo la cupola.");
        } else if (SpegniS[2].s == ISS_ON) {
            digitalWrite(SPEGNI_PRIMO_PIN, HIGH);
            digitalWrite(SPEGNI_SECONDO_PIN, LOW);
            sleep(1);
            digitalWrite(SPEGNI_SECONDO_PIN, HIGH);
            LOG_INFO("Sto accendendo tutto");
            IDSetSwitch(&SpegniSP, "Sto accendendo tutto.");
        }
	return true;
    } else if (!strcmp(name, BluetoothSP.name)) {
	IUResetSwitch(&BluetoothSP);
        system("sudo /bin/systemctl restart rfcomm.service");
        IDSetSwitch(&BluetoothSP, "Riavvio bluetooth");
	return true;
    } else if (!strcmp(name, RiavviaPiSP.name)) {
	IUResetSwitch(&RiavviaPiSP);
        IDSetSwitch(&RiavviaPiSP, "Riavvio il sistema");
        system("sudo /sbin/reboot");
	return true;
    }
    return INDI::Dome::ISNewSwitch(dev, name, states, names, n);
}

bool OpenLoopDome::ISSnoopDevice (XMLEle *root)
{
   const char *propName = findXMLAttValu(root, "name");
   if (!strcmp(propName, "AAG Cloud Watcher")) {
      XMLEle *cloudSwitch = findXMLEle(root, "overcast");
      XMLEle *rainSwitch = findXMLEle(root, "dry");
      if (!strcmp(pcdataXMLEle(cloudSwitch), "On") || !strcmp(pcdataXMLEle(rainSwitch), "Off")){
          LOG_INFO("IL CLOUD WATCHER NON VUOLE CHE APRI");
    	  Dome::ControlShutter(SHUTTER_CLOSE);
      }
      else if(!strcmp(pcdataXMLEle(cloudSwitch), "Off") && !strcmp(pcdataXMLEle(rainSwitch), "On")){
          LOG_INFO("Ora puoi aprire di nuovo");
      }
      return true;
   }
   return INDI::Dome::ISSnoopDevice(root); 
}
